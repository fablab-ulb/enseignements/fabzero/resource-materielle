# Magasin dans le coin

Brico 
 * Avenue de la Couronne 330 1050 Ixelles 
 * [Ouverture](https://www.brico.be/fr/storedetail/3464/ixelles-elsene)

Cubotex
 * magasin d'éléctronique
 * Av. des Saisons 100, 1050 Bruxelles
 * [Site Web](https://www.cotubex.be/fr/)

International Tools Service I.T.S.
 * Outillage & Quincaillerie
 * Rue du Printemps 39/43, 1050 Ixelles
 * [Site Web](https://its-tools.com/fr/)

# Magasin dans BXL

Schleiper
 * Magasin de fournitures pour les métiers d'art et les loisirs créatifs
 * Schleiper, Charleroise Steenweg 149, 1060 Brussel
 * Cadres Schleiper, Rue de l'Etang 75, 1040 Etterbeek
 * [Site Web](https://www.schleiper.com/)

# Site Online

RS
 * Outillage et éléctronique
 * [Site Web](https://befr.rs-online.com/web/)

# Spécifiquement pour la laser

Trotec
 * Matérieux pour la découp et/ou gravure 
 * [Site Web](https://shop.troteclaser.com/s/?language=en_US)

# Bois pour CNC

Rotor DeConstruction
* bois de récup
* https://rotordc.com/

In Limbo
* bois de récup
* https://toestand.be/en/projects/in-limbo

Gede Bois
* https://gedebois.be/

Coopérative foret de Soignes - Sonian Wood Coop
* https://sonian.brussels/
